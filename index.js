let sidebar_chat = document.querySelectorAll(".sidebar-chat");

sidebar_chat.forEach(function (chat) {
    chat.addEventListener("click", function () {
        let current = document.getElementsByClassName("active");
        if (current.length > 0) {
            current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";

        document.getElementById("contact-name").innerText = chat.querySelector("h4").innerText;
        document.getElementById("image-source").src = chat.querySelector("img").src;

        let screenWidth = screen.width;
        if (screenWidth >= 429) {
            return;
        }
        let side_bar = document.querySelector(".sidebar")
        side_bar.style.display = 'none';
        let message_container = document.querySelector(".message-container");
        message_container.style.display = 'block';

        let newDiv = document.createElement("div");
        newDiv.className = "chat-header-right";
        let a = document.getElementById("insert-new-div");
        let b = document.getElementById("insert-before");
        a.insertBefore(newDiv, b);
        document.getElementById("mobile-video-call-icon").src = "video-call.png";
        document.getElementById("mobile-audio-call-icon").src = "audio-call.svg";
    });
});




















